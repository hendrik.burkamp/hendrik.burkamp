# hendrik.burkamp

[Here](https://gitlab.com/hburkamp) is where I host my personal projects.
Some of them are even public.
Like my [dotfiles](https://gitlab.com/hburkamp/dotfiles) or a statically hosted [map](https://hburkamp.gitlab.io/mhp_tools/map_tool/) I made.
I also have a [GitHub](https://github.com/hburkamp) profile.
But I use that mainly to fork my [keyboard layout](https://github.com/hburkamp/miryoku_qmk).
